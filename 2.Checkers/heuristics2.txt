package checkers; // This package is required - don't remove it
public class EvaluatePosition // This class is required - don't remove it
{
	static private final int WIN=Integer.MAX_VALUE/2;
	static private final int LOSE=Integer.MIN_VALUE/2;
	static private boolean _color; // This field is required - don't remove it
	static public void changeColor(boolean color) // This method is required - don't remove it
	{
		_color=color;
	}
	static public boolean getColor() // This method is required - don't remove it
	{
		return _color;
	}
	static public int evaluatePosition(AIBoard board) // This method is required and it is the major heuristic method - type your code here
	{
		int myRating=0;
		int opponentsRating=0;
		int size=board.getSize();

		int wages[][] = {{0,4,0,4,0,4,0,4},
						{4,0,3,0,3,0,3,0},
						{0,3,0,2,0,2,0,4},
						{4,0,2,0,1,0,3,0},
						{0,3,0,1,0,2,0,4},
						{4,0,2,0,1,0,3,0},
						{0,3,0,2,0,2,0,4},
						{4,0,4,0,4,0,4,0}};

      for (int i=0;i<size;i++)
		  {
			      for (int j=(i+1)%2;j<size;j+=2)
			      {
				      if (!board._board[i][j].empty) // field is not empty
				      {
					      if (board._board[i][j].white==getColor()) // this is my piece
					      {

							if(i!=0 && j!=0 && i!=size-1 && j!=size-1)
							{

							  if(!board._board[i+1][j+1].white==getColor() || 
							     !board._board[i+1][j-1].white==getColor() ||
							     !board._board[i-1][j+1].white==getColor() || 
							     !board._board[i-1][j-1].white==getColor() )
							  {
						        	if (board._board[i][j].king) myRating+=2; 
						        	else myRating+=1; // this is my piece
							  }
							}
                  				myRating+=wages[i][j];
					      }
					      else
					      {
							if(i!=0 && j!=0 && i!=size-1 && j!=size-1)
							{
							  if(board._board[i+1][j+1].white==getColor() || 
							     board._board[i+1][j-1].white==getColor() ||
							     board._board[i-1][j+1].white==getColor() || 
							     board._board[i-1][j-1].white==getColor() )
							  {
						        	if (board._board[i][j].king) opponentsRating+=2;
						        	else opponentsRating+=1;
							  }
							}
                  				opponentsRating+=wages[i][j];
					      }
				      }
			      }
		  }
		//Judge.updateLog(Integer.toString(myRating)+" - "+Integer.toString(opponentsRating)+" =  "+Integer.toString(myRating-opponentsRating)+"\n");
		if (myRating==0) return LOSE; 
		else if (opponentsRating==0) return WIN; 
		else return myRating-opponentsRating;
	}
}

