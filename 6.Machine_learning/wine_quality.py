import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler

from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline

from sklearn.model_selection import KFold, StratifiedKFold, \
    cross_val_score, train_test_split, \
    learning_curve, validation_curve, \
    GridSearchCV

from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB

from visualization import plot_feature_importances, \
    plot_bias_variance_graph, plot_decision_regions

format_line = 60
format_width = 50
format_prec = .4
cv_split = 5



def check_accuracy_full(clf,X,y,print_text):
    sk_fold = StratifiedKFold(n_splits=cv_split)
    scores = cross_val_score(clf,
                         X,
                         y,
                         cv=sk_fold,
                         n_jobs=-1)

    print('|{:<{width}}| {:{prec}f}|'.format(print_text,
                                            (np.mean(scores)) ,
                                            width = format_width,
                                            prec = format_prec))

    # clf.fit(X,y)
    # y_pred = clf.predict(X)
    # print('{:<{width}}: {:{prec}f}'.format(print_text,
    #                                         accuracy_score(y,y_pred),
    #                                         width = format_width,
    #                                         prec = format_prec))

def check_accuracy_train_test(clf,X_train,X_test,y_train,y_test,print_text):
    sk_fold = StratifiedKFold(n_splits=cv_split)
    scores = cross_val_score(clf,
                         X_train,
                         y_train,
                         cv=sk_fold,
                         n_jobs=-1)

    print('|{:<{width}}| {:{prec}f}|'.format(print_text,
                                            (np.mean(scores)) ,
                                            width = format_width,
                                            prec = format_prec))
    # clf.fit(X_train,y_train)
    # y_pred = clf.predict(X_test)
    # print('{:<{width}}: {:{prec}f}'.format(print_text,
    #                                        accuracy_score(y_test, y_pred),
    #                                         width = format_width,
    #                                         prec = format_prec))

def check_accuracy_pipe(pipe,X_train,X_test,y_train,y_test,print_text):

    sk_fold = StratifiedKFold(n_splits=cv_split)
    scores = cross_val_score(pipe,
                         X_train,
                         y_train,
                         cv=sk_fold,
                         n_jobs=-1)

    print('|{:<{width}}| {:{prec}f}|'.format(print_text,
                                            (np.mean(scores)) ,
                                            width = format_width,
                                            prec = format_prec))
    # pipe.fit(X_train,y_train)
    # print('{:<{width}}: {:{prec}f}'.format(print_text,
    #                                         pipe.score(X_test,y_test),
    #                                         width = format_width,
    #                                         prec = format_prec))

def cross_val_acc(clf,X,y,print_text):
    sk_fold = StratifiedKFold(n_splits=10)
    scores = cross_val_score(clf,
                         X,
                         y,
                         cv=sk_fold,
                         n_jobs=-1)

    print('{|<{width}}| {:{prec}f}|'.format(print_text,
                                            (sum(scores)/len(scores)) ,
                                            width = format_width,
                                            prec = format_prec))

def full_data_set(X,y):

    naive_bayes = GaussianNB()
    neighbors = KNeighborsClassifier(n_neighbors=5,n_jobs=-1)
    kernelSVM = SVC(kernel='rbf',C=1.0,random_state=0)

    check_accuracy_full(naive_bayes,X,y,'Naive bayes full data set')
    check_accuracy_full(neighbors,X,y,'KNeighbors full data set (n=5)')
    check_accuracy_full(kernelSVM,X,y,'KernelSVM full data set (C=1.0)')

    print("")

def train_test_data_set(X_train,X_test,y_train,y_test):
    naive_bayes = GaussianNB()
    neighbors = KNeighborsClassifier(n_neighbors=5,n_jobs=-1)
    kernelSVM = SVC(kernel='rbf',C=1.0,random_state=0)

    check_accuracy_train_test(naive_bayes,X_train,X_test,
                              y_train,y_test,'Naive train test data set')
    check_accuracy_train_test(neighbors,X_train,X_test,
                              y_train,y_test,'KNeighbors train test data set (n=5)')
    check_accuracy_train_test(kernelSVM,X_train,X_test,
                              y_train,y_test,'KernelSVM train test data set (C=1.0)')
    print("")

def pipe_train_test(X_train,X_test,y_train,y_test):
    pipe_naive = Pipeline([('sc',StandardScaler()),
                       ('clf',GaussianNB())]
                      )

    pipe_neigh = Pipeline([('sc',StandardScaler()),
                       ('clf',KNeighborsClassifier(n_neighbors=5,n_jobs=-1))]
                      )

    pipe_svm = Pipeline([('sc',StandardScaler()),
                   ('clf',SVC(kernel='rbf',C=1.0,random_state=0))]
                  )

    check_accuracy_pipe(pipe_naive,X_train,X_test,
                              y_train,y_test,'Naive std train test ')

    check_accuracy_pipe(pipe_neigh,X_train,X_test,
                              y_train,y_test,'KNeighbors (n=5) std train test ')

    check_accuracy_pipe(pipe_svm,X_train,X_test,
                              y_train,y_test,'KernelSVM (C=1.0) std train test ')

    print("")


def pipe_PCA_train_test(X_train,X_test,y_train,y_test):
    pipe_naive = Pipeline([('sc',StandardScaler()),
                       ('pca', PCA(n_components=2)),
                       ('clf',GaussianNB())]
                      )

    pipe_neigh = Pipeline([('sc',StandardScaler()),
                       ('pca', PCA(n_components=2)),
                       ('clf',KNeighborsClassifier(n_neighbors=5,n_jobs=-1))]
                      )

    pipe_svm = Pipeline([('sc',StandardScaler()),
                       ('pca', PCA(n_components=2)),
                   ('clf',SVC(kernel='rbf',C=1.0,random_state=0))]
                  )

    check_accuracy_pipe(pipe_naive,X_train,X_test,
                              y_train,y_test,'Naive std pca (n=2) train test ')

    check_accuracy_pipe(pipe_neigh,X_train,X_test,
                              y_train,y_test,'KNeighbors (n=5) std pca (n=2) train test')

    check_accuracy_pipe(pipe_svm,X_train,X_test,
                              y_train,y_test,'KernelSVM (C=1.0) std pca (n=2) train test ')

    print("")


def pipe_learning_curve(pipe,X_train,y_train,title):
    train_sizes, train_scores, test_scores = learning_curve(pipe,
                                                            X_train,
                                                            y_train,
                                                            train_sizes=np.linspace(0.1,1.0,20),
                                                            cv=cv_split
                                                            )
    plot_bias_variance_graph(train_sizes,train_scores,test_scores)
    plt.title(title)

    print("")


if __name__ == "__main__":

    print("{:_^{line}s}".format('START',line=format_line))

    ## load data
    df = pd.read_csv('winequality-red.csv', sep=';',header=None)
    df.columns = ["fixed acidity","volatile acidity",
                  "citric acid","residual sugar",
                  "chlorides","free sulfur dioxide",
                  "total sulfur dioxide",
                  "density","pH",
                  "sulphates","alcohol","quality"]

    print("{:_^{line}s}".format('Data size',line=format_line))
    print("{:d} instances, {:d} features, 10 classes".format(df.shape[0],df.shape[1]-1))
    print("")
    raw_input()


    X, y = df.iloc[:, :-1].values, df.iloc[:, -1].values

    print("{:_^{line}s}".format('Clean data set',line=format_line))
    full_data_set(X,y)
    X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.3)
    raw_input()


    ## Standardization
    # sc = StandardScaler()
    # sc.fit(X)
    # X_std = sc.transform(X)

    # print("Standardized")
    # full_data_set(X_std,y)
    # raw_input()

    # print("Test size 0.3")
    # train_test_data_set(X_train,X_test,y_train,y_test)
    # raw_input()

    print("{:_^{line}s}".format('Standardization test set 0.3',line=format_line))
    pipe_train_test(X_train,X_test,y_train,y_test)
    raw_input()

    print("{:_^{line}s}".format('Feature importance',line=format_line))
    # plt.figure(1)
    # importances,indices =  plot_feature_importances(X,y,df.columns[:-1])
    # plt.show(block=False)
    importances = [ 0.07547573,  0.10357205,  0.07417463,  0.07092446,  0.08121375,  0.06676116,
                    0.10398046,  0.0921751,   0.07476587,  0.1106847,   0.14627207]
    indices = [10,  9,  6,  1,  7,  4,  0,  8 , 2,  3,  5]

    for i in xrange(X.shape[1]):
        print("{:>2d}) {:<20s} {:.4f}".format(i+1,df.columns[:-1][indices[i]],importances[indices[i]]))

    print("")

    print("{:_^{line}s}".format('Feature selection',line=format_line))
    all_indices = 8
    most_important = [indices[i] for i in range(all_indices)]
    X_selected = df.iloc[:, most_important].values ## feature selection
    X_train, X_test, y_train, y_test = train_test_split(X_selected,y,test_size=0.3)

    pipe_train_test(X_train,X_test,y_train,y_test)
    pipe_PCA_train_test(X_train,X_test,y_train,y_test)
    raw_input()

    print("{:_^{line}s}".format('Class Reduction',line=format_line))
    ## reduce no of classes <=5 bad, 6 ok, >=7 good
    print("|{:^5s}|{:^10s}|".format('class','no of instances'))
    for i in xrange(10):
        print("|{:^5d}|{:^15d}|".format(i+1,
                                     np.shape(np.where( df.iloc[:, -1].values == i+1 ))[1]))
    print("{:-^{line}s}".format('',line=23))


    y_reduced = y.copy()
    bad = np.where( df.iloc[:, -1].values <= 5)
    ok = np.where( df.iloc[:, -1].values == 6 )
    good = np.where( df.iloc[:, -1].values >= 7)
    y_reduced[bad] = 0
    y_reduced[ok] = 1
    y_reduced[good] = 2

    X_train, X_test, y_train, y_test = train_test_split(X_selected,y_reduced,test_size=0.3)
    pipe_train_test(X_train,X_test,y_train,y_test)
    pipe_PCA_train_test(X_train,X_test,y_train,y_test)

    raw_input()

    print("{:_^{line}s}".format('Naive Bayes pipe Tunning',line=format_line))
    pca = PCA(n_components=2)
    naive = GaussianNB()

    pipe_naive = Pipeline([('sc',StandardScaler()),
                       ('pca', pca),
                       ('clf',naive)]
                      )

    param_grid = [{'pca__n_components':[i for i in xrange(2,all_indices)]}]
    gs_naive = GridSearchCV(estimator=pipe_naive,
                      param_grid=param_grid,
                      scoring='accuracy',
                      cv=cv_split,
                      n_jobs=-1)

    gs_naive.fit(X_train,y_train)
    print('Accuracy   : {:.4f}'.format(gs_naive.best_score_))
    print('Best params: {:s}'.format(gs_naive.best_params_))
    print("")



    print("{:_^{line}s}".format('KNeighbors pipe Tunning',line=format_line))
    pca = PCA()
    neigh = KNeighborsClassifier()

    pipe_neigh = Pipeline([('sc',StandardScaler()),
                       ('pca', pca),
                       ('clf',neigh)]
                      )

    param_grid = [{'pca__n_components':[i for i in xrange(2,all_indices)],
                   'clf__n_neighbors' :[i for i in xrange(2,10)]}]
    gs_neigh = GridSearchCV(estimator=pipe_neigh,
                      param_grid=param_grid,
                      scoring='accuracy',
                      cv=cv_split,
                      n_jobs=-1)

    gs_neigh.fit(X_train,y_train)
    print('Accuracy   : {:.4f}'.format(gs_neigh.best_score_))
    print('Best params: {:s}'.format(gs_neigh.best_params_))
    print("")


    print("{:_^{line}s}".format('KernelSVM pipe Tunning',line=format_line))
    pca = PCA()
    svm = SVC(kernel='rbf')


    pipe_svm = Pipeline([('sc',StandardScaler()),
                       ('pca', pca),
                   ('clf',svm)]
                  )
    param_range = np.logspace(-2,3,6)
    param_grid = [{'pca__n_components':[i for i in xrange(2,all_indices)],
                   'clf__C' :param_range}]
    gs_svm = GridSearchCV(estimator=pipe_svm,
                      param_grid=param_grid,
                      scoring='accuracy',
                      cv=cv_split,
                      n_jobs=-1)

    gs_svm.fit(X_train,y_train)
    print('Accuracy   : {:.4f}'.format(gs_svm.best_score_))
    print('Best params: {:s}'.format(gs_svm.best_params_))
    print("")

    raw_input()
    print("{:_^{line}s}".format('SUMMARY',line=format_line))
    print("{:_^{line}s}".format('First models',line=format_line))
    full_data_set(X,y)
    print("{:_^{line}s}".format('The best models',line=format_line))
    pipe_naive_best = Pipeline([('sc',StandardScaler()),
                       ('pca', PCA(n_components=gs_naive.best_params_['pca__n_components'])),
                       ('clf',GaussianNB())]
                      )

    pipe_neigh_best = Pipeline([('sc',StandardScaler()),
                       ('pca', PCA(n_components=gs_neigh.best_params_['pca__n_components'])),
                       ('clf',KNeighborsClassifier(n_neighbors=gs_neigh.best_params_['clf__n_neighbors'],
                                                   n_jobs=-1))]
                      )

    pipe_svm_best = Pipeline([('sc',StandardScaler()),
                       ('pca', PCA(n_components=gs_svm.best_params_['pca__n_components'])),
                   ('clf',SVC(kernel='rbf',
                              C=gs_svm.best_params_['clf__C'],
                              random_state=0))]
                  )

    check_accuracy_pipe(pipe_naive_best,X_train,X_test,
                              y_train,y_test,'Naive best std train test ')

    check_accuracy_pipe(pipe_neigh_best,X_train,X_test,
                              y_train,y_test,'KNeighbors best std train test ')

    check_accuracy_pipe(pipe_svm_best,X_train,X_test,
                              y_train,y_test,'KernelSVM best std train test ')


    print("{:_^{line}s}".format('Learning curve',line=format_line))
    plt.figure(2)
    train_sizes, train_scores, test_scores = learning_curve(pipe_svm_best,
                                                            X,
                                                            y,
                                                            train_sizes=np.linspace(0.1,1.0,20),
                                                            cv=cv_split
                                                            )

    plot_bias_variance_graph(train_sizes,train_scores,test_scores)
    print("{:_^{line}s}".format('END',line=format_line))
    plt.show()