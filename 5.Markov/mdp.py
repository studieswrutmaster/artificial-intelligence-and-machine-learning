import operator
import matplotlib.pyplot as plt
import numpy as np
import random

class MDP:
    def __init__(self,param):
        # define terminals states
        self.terminals = ((param['T'][0][0],param['T'][0][1]),)
        for i in range(1,len(param['T'])):
            self.terminals = self.terminals + ((param['T'][i][0],param['T'][i][1]),)

        ## define gamma
        self.gamma = param['gamma']

        self.p1 = param['p1']
        self.p2 = param['p2']
        self.p3 = param['p3']

        self.iterations = 0
        self.init = (2,2)
        self.orientations = [(1, 0), (0, 1), (-1, 0), (0, -1)]#right,up,left,down action
        if not (0 <= self.gamma <= 1):
            raise ValueError("An MDP must have 0 <= gamma < 1")
        self.states = set()
        self.reward = {}

        self.world = self.create_world(param)
        self.rows = len(self.world)
        self.cols = len(self.world[0])
        for x in range(self.cols):
            for y in range(self.rows):
                self.reward[x, y] = self.world[y][x]
                if self.world[y][x] is not None:
                    self.states.add((x, y))
        self.world.reverse()

        self.convergence_plot = {s:[] for s in self.states}
        self.convergence_plot['iterations'] = []

        self.utility = {}
        self.policy = {}
        self.Q_values = {}
        self.epsilon = param['epsilon']

    def turn_heading(self,heading, inc):
        return self.orientations[(self.orientations.index(heading) + inc) % len(self.orientations)]


    def turn_right(self,heading):
        return self.turn_heading(heading, -1)


    def turn_left(self,heading):
        return self.turn_heading(heading, +1)

    def go_down(self,heading):
        return self.turn_heading(-1, heading)

    def vector_add(self,a, b):
        """Component-wise addition of two vectors."""
        return tuple(map(operator.add, a, b))

    def translate_action(self,action):
        if action == (1,0): return 'right'
        if action == (0,1): return 'up'
        if action == (-1,0): return 'left'
        if action == (0,-1): return 'down'


    def R(self, state):
        "Return a numeric reward for this state."
        return self.reward[state]

    def T(self, state, action):
        if action is None:
            return [(0.0, state)]
        else:
            return [(self.p1, self.go(state, action)),
                    (self.p2, self.go(state, self.turn_right(action))),
                    (self.p3, self.go(state, self.turn_left(action)))]

    def actions(self, state):
        if state in self.terminals:
            return [None]
        else:
            return self.orientations

    def go(self, state, direction):
        "Return the state that results from going in this direction."
        state1 = self.vector_add(state, direction)
        return state1 if state1 in self.states else state


    def go_agent(self, state, action):
        value = np.random.random()
        if value < self.p1:
            return self.go(state,action)
        elif (value < self.p2+self.p1 and value > self.p1):
            return self.go(state,self.turn_right(action))
        elif (value > self.p2+self.p1 and value < self.p1-self.p2-self.p3):
            return self.go(state,self.turn_left(action))
        else:
            return self.go(state,self.go_down(action))

    def to_grid(self, mapping):
        """Convert a mapping from (x, y) to v into a [[..., v, ...]] grid."""
        return list(reversed([[mapping.get((x, y), None)
                               for x in range(self.cols)]
                              for y in range(self.rows)]))

    def to_arrows(self):
        chars = {
            (1, 0): '>', (0, 1): '^', (-1, 0): '<', (0, -1): 'v', None: '.'}
        return self.to_grid({s: chars[a] for (s, a) in self.policy.items()})

    def to_utilities(self):
        return self.to_grid({s: "{0:.4f}".format(u) for (s,u) in self.utility.items()})

    def plot_convergence(self,file_name=None):
        f = plt.figure(figsize=(5.4,5.4))
        ax = plt.subplot(111)
        num_plots = len(self.states)
        colormap = plt.cm.gist_ncar
        plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, 0.9, num_plots)])

        for keys in self.states:
            ax.plot(self.convergence_plot['iterations'],self.convergence_plot[keys],label=keys)
            # plt.text(self.convergence_plot['iterations'][-1],
            #          self.convergence_plot[keys][-1],
            #          keys)

        ymin = min(min(self.convergence_plot[key] for key in self.convergence_plot.keys() if key not in ['iterations']))
        ymax = max(max(self.convergence_plot[key] for key in self.convergence_plot.keys() if key not in ['iterations']))
        plt.title('Convergence')
        plt.ylim(ymin-abs(ymin/10),ymax + abs(ymax/10))
        plt.xlim(self.convergence_plot['iterations'][0],self.convergence_plot['iterations'][-1])
        plt.ylabel('Utility estimates')
        plt.xlabel('Number of iterations')
        plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # plt.legend(bbox_to_anchor=(1.05, 1), loc=2)
        if file_name==None:
            f.show()
        else:
            f.savefig(file_name)

    def create_world(self,param):
        grid = [[param['r']] * param['N'] for i in range(param['M'])]

        for i in range(len(param['F'])):
            # x,y = param['F'][i][0]
            x = param['F'][i][0]
            y = param['F'][i][1]
            grid[y][x] = None

        for i in range(len(param['B'])):
            # x,y = param['B'][i][0]
            x = param['B'][i][0]
            y = param['B'][i][1]
            grid[y][x] = param['B'][i][2]

        for i in range(len(param['T'])):
            # x,y = param['T'][i][0]
            x = param['T'][i][0]
            y = param['T'][i][1]
            grid[y][x] = param['T'][i][2]

        return grid


    def value_iteration(mdp):
        U1 = {s: 0 for s in mdp.states}
        R, T, gamma = mdp.R, mdp.T, mdp.gamma
        delta = 1
        if mdp.gamma == 1.0:
            for i in range(30):
                U = U1.copy()
                mdp.iterations += 1
                # print("iteration"+str(mdp.iterations))
                mdp.convergence_plot['iterations'].append(mdp.iterations)
                for s in mdp.states:
                    # Q = [sum([p * U[s1] for (p, s1) in T(s, a)]) for a in mdp.actions(s)]# up, right, down, left
                    # print(s,Q)
                    # U1[s] = R(s) + gamma * max(Q)
                    Q = [R(s)+ gamma * sum([p * U[s1] for (p, s1) in T(s, a)]) for a in mdp.actions(s)]# right, up, left, down
                    # print(s,["%.2f"%Q[i] for i in range(len(Q))])# right, up, left, down
                    mdp.Q_values[s]=Q
                    U1[s] = max(Q)
                    # print s
                    mdp.convergence_plot[s].append(U1[s])
                    # print mdp.convergence_plot[s]

        else:
            while not(delta < mdp.epsilon * (1 - gamma) / gamma):
                U = U1.copy()
                delta = 0
                mdp.iterations += 1
                mdp.convergence_plot['iterations'].append(mdp.iterations)
                # print "iteration no.",mdp.iterations
                for s in mdp.states:
                    Q = [R(s)+ gamma * sum([p * U[s1] for (p, s1) in T(s, a)]) for a in mdp.actions(s)]# right, up, left, down
                    mdp.Q_values[s]=Q
                    U1[s] = max(Q)

                    # U1[s] = R(s) + gamma * max([sum([p * U[s1] for (p, s1) in T(s, a)])
                    #                             for a in mdp.actions(s)])
                    # print mdp.convergence_plot[(0,0)]
                    mdp.convergence_plot[s].append(U1[s])
                    delta = max(delta, abs(U1[s] - U[s]))
                    # print mdp.convergence_plot[s]


        mdp.utility = U

    def best_policy(mdp):
        pi = {}
        for s in mdp.states:
            pi[s] = max(mdp.actions(s), key=lambda a: mdp.expected_utility(a, s, mdp.utility, mdp))
        mdp.policy = pi


    def expected_utility(self,a, s, U, mdp):
        "The expected utility of doing a in state s, according to the MDP and U."
        return sum([p * U[s1] for (p, s1) in mdp.T(s, a)])


    def print_table(mdp,clust_data,title,file_name=None):
        f = plt.figure(figsize=(2.5,2.0))
        plt.axis('tight')
        plt.axis('off')
        plt.table(cellText=clust_data,
                  cellLoc='center',
                  bbox=[0,0,1.0,1.0]
                  )
        plt.title(title)
        if file_name==None:
            f.show()
        else:
            f.savefig(file_name)

    def show_world(mdp,file_name):
        mdp.print_table(mdp.world,'World',file_name)
        # print(np.array(mdp.world))
    def print_world(self):
        print("World")
        self.print_console(self.world)


    def print_console(self,data):
        rows = len(data)
        cols = len(data[0])
        for i in range(rows):
            print("-------------------------------------------------------------")

            for j in range(cols):
                # if (j,i) in data.keys():
                #     value = data[(j,i)]
                #     print("|"+repr("{0:.4f}".format(value)).center(10)),
                # else:
                #     value = None
                    print("|"+repr(data[i][j]).center(10)),
            print("|")
        print("-------------------------------------------------------------")

    def get_value_cell(self,i,j):
        if (j,i) in self.Q_values.keys():
            value = self.Q_values[(j,i)]
        else:
            value = [None]
        return value

    def print_Q_values(self):
        print("Q values")
        rows = self.rows
        cols = self.cols
        width = 20
        for i in range(rows-1,-1,-1):
            print("---------------------------------------------------------------------------------------------------------------")

            for j in range(cols):
                value = self.get_value_cell(i,j)
                if len(value)==4:
                    print("|"+repr("%.4f"%value[1]).center(width)),
                else:
                    print("|"+repr(" ").center(width)),

            print("|")
            for j in range(cols):
                value = self.get_value_cell(i,j)
                if len(value)==4:
                    print("|"+repr("%.4f"%value[2]).ljust(width/2)+repr("%.4f"%value[0]).rjust(width/2)),
                else:
                    if value[0] == None:    print("|"+repr("None").center(width)),
                    else:    print("|"+repr("%.4f"%value[0]).center(width)),
            print("|")
            for j in range(cols):
                value = self.get_value_cell(i,j)
                if len(value)==4:
                    print("|"+repr("%.4f"%value[3]).center(width)),
                else:
                    print("|"+repr(" ").center(width)),
            print("|")
        print("---------------------------------------------------------------------------------------------------------------")

    def print_utilities(self):
        print("Utilities")
        self.print_console(self.to_utilities())

    def show_utilities(mdp,file_name):
        mdp.print_table(mdp.to_utilities(),'Utilities',file_name)

    def show_policy(mdp,file_name):
        mdp.print_table(mdp.to_arrows(),'Policy',file_name)

    def print_policy(self):
        print("Policy")
        self.print_console(self.to_arrows())

    def show_results(mdp):
        mdp.value_iteration()
        mdp.best_policy()

        print("Iterations "+str(mdp.iterations))
        mdp.show_world()
        mdp.show_utilities()
        mdp.show_policy()
        mdp.plot_convergence()
        print("")

    def save_results(mdp,dir_name):
        mdp.value_iteration()
        mdp.best_policy()

        print("Iterations "+str(mdp.iterations))
        # mdp.show_world(dir_name+'/1World')
        mdp.print_world()
        # mdp.show_utilities(dir_name+'/2Utilities')
        mdp.print_utilities()
        # mdp.show_policy(dir_name+'/3Policy')
        mdp.print_policy()
        mdp.plot_convergence(dir_name+'/4Convergence')
        mdp.print_Q_values()
        print("")