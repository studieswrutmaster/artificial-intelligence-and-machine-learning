from mdp import MDP
import yaml

def save_param(obj, name ,dir_name):
    # with open(dir_name+'/'+ name + '.pkl', 'wb') as f:
    #     pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
    with open(dir_name+'/'+ name + '.yaml', 'w') as yamlfile:
        yaml.dump(obj, yamlfile, default_flow_style=False)

def load_param(name ,dir_name):
    with open(dir_name+'/' + name + '.yaml', 'r') as f:
        # return pickle.load(f)
        return yaml.load(f)

def markov_decision_problem(param, dir_name=None):
    print("*****START "+dir_name+"*****")
    save_param(param,'parameters','mdp_'+dir_name)
    mdp = MDP(param)
    if dir_name==None:
        mdp.show_results()
    else:
        mdp.save_results('mdp_'+dir_name)

    print("*****END "+dir_name+"*******\n")

def example1(): ##original task
    ## parameters, set as dictionary
    param = {}
    param['N'] = 4
    param['M'] = 3
    param['p1'] = 0.8 #probability go up
    param['p2'] = 0.1 #probability go right
    param['p3'] = 0.1 #probability go left
    param['r'] = -0.04 #reward
    param['gamma'] = 1.0 #discoupling
    param['epsilon'] = 0.0001

    param['F'] = [[1,1]] #forbidden
    param['B'] = [] #special
    param['T'] = [[3,2,1],[3,1,-1]] #terminal

    ## or load from yaml file
    # param = load_param('parameters','mdp_ex1')

    markov_decision_problem(param,'ex1')

def example2():
    ## parameters
    param = {}
    param['N'] = 4
    param['M'] = 4
    param['p1'] = 0.8 #probability go up
    param['p2'] = 0.1 #probability go right
    param['p3'] = 0.1 #probability go left
    param['r'] = -1
    param['gamma'] = 0.99
    param['epsilon'] = 0.0001

    param['F'] = [[2,0]]
    param['B'] = [[2,1,-20]]
    param['T'] = [[3,0,100]]

    markov_decision_problem(param,'ex2')

def example3(): ## example2
    ## parameters
    param = {}
    param['N'] = 4
    param['M'] = 4
    param['p1'] = 0.8 #probability go up
    param['p2'] = 0.1 #probability go right
    param['p3'] = 0.1 #probability go left
    param['r'] = 0
    param['gamma'] = 0.99
    param['epsilon'] = 0.0001

    param['F'] = [[2,0]]
    param['B'] = [[2,1,-50]]
    param['T'] = [[3,0,50]]

    markov_decision_problem(param,'ex3')

def example4(): ##example2
    ## parameters
    param = {}
    param['N'] = 4
    param['M'] = 4
    param['p1'] = 0.3 #probability go up
    param['p2'] = 0.35 #probability go right
    param['p3'] = 0.35 #probability go left
    param['r'] = -1
    param['gamma'] = 0.99
    param['epsilon'] = 0.0001

    param['F'] = [[2,0]]
    param['B'] = [[2,1,-20]]
    param['T'] = [[3,0,100]]

    markov_decision_problem(param,'ex4')

def example5(): ##original task
    ## parameters
    param = {}
    param['N'] = 4
    param['M'] = 4
    param['p1'] = 0.8 #probability go up
    param['p2'] = 0.1 #probability go right
    param['p3'] = 0.1 #probability go left
    param['r'] = -1
    param['gamma'] = 0.2
    param['epsilon'] = 0.0001

    param['F'] = [[2,0]]
    param['B'] = [[2,1,-20]]
    param['T'] = [[3,0,100]]

    markov_decision_problem(param,'ex5')

def example5x5():
    ## parameters
    param = {}
    param['N'] = 5
    param['M'] = 5
    param['p1'] = 0.6 #probability go up
    param['p2'] = 0.2 #probability go right
    param['p3'] = 0.2 #probability go left
    param['r'] = -1
    param['gamma'] = 0.99
    param['epsilon'] = 0.25

    param['F'] = []
    param['B'] = [[1,2,-10],[1,3,-20],[2,1,-5],[2,3,-5],[3,1,-20],[3,2,-10]]
    param['T'] = [[0,0,-50],[0,4,100],[4,0,100],[4,4,-50]]

    markov_decision_problem(param,'5x5')

if __name__ == "__main__":
    # example1()
    # example2()
    # example3()
    # example4()
    # example5()
    example5x5()


