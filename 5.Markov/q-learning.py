from mdp import MDP
import random
import numpy as np
# import scipy as sp
# from scipy.linalg import norm

class Simulator(MDP):
    def __init__(self,param):
        MDP.__init__(self,param)



class Agent:
    def __init__(self,param):
        self.k = 0
        self.actions = param['actions']
        self.states = param['states']
        self.N_dict = {s:[0.,0.,0.,0.] for s in self.states}
        self.Q_dict = {s:[0.,0.,0.,0.] for s in self.states}
        self.terminal_states = param['terminal_states']
        for i in self.terminal_states:
            self.Q_dict[i] = [0]
            self.N_dict[i] = [0]

        self.states_num = {}
        i = 0
        for s in self.states:
            self.states_num[s] = i
            i+=1
        self.actions_num = {}
        i = 0
        for s in self.actions:
            self.actions_num[s] = i
            i+=1
        self.epsilon = param['epsilon']
        self.rows = param['rows']
        self.cols = param['cols']
        self.init_state = param['init_state']

    def f(self,u,n):
        if self.k == 0: return u
        else: return u+self.k/n

    def get_action_number(self,action):
        if action == (1,0): return 0
        if action == (0,1): return 1
        if action == (-1,0): return 2
        if action == (0,-1): return 3

    def get_state(self,state):
        return self.states_num[state]

    def N_increment(self,state,action):
        act = self.N_dict[state]
        if state in self.terminal_states:
            pass
        else:
            act[self.get_action_number(action)] += 1

    def N(self,state,action):
        act = self.N_dict[state]
        if state in self.terminal_states:
            return 1
        else:
            return act[self.get_action_number(action)]

    def Q(self,state,action):
        act = self.Q_dict[state]
        if state in self.terminal_states:
            return act[0]
        else:
            return act[self.get_action_number(action)]

    def Q_update(self,state,action,value):
        act = self.Q_dict[state]
        if state in self.terminal_states:
            self.Q_dict[state] = [value]
        else:
            act[self.get_action_number(action)] = value

    def get_value_cell(self,i,j):
        if (j,i) in self.Q_dict.keys():
            value = self.Q_dict[(j,i)]
        else:
            value = [None]
        return value

    def print_Q(self):
        print("Q values")
        rows = self.rows
        cols = self.cols
        width = 20
        for i in range(rows-1,-1,-1):
            print("---------------------------------------------------------------------------------------------------------------")

            for j in range(cols):
                value = self.get_value_cell(i,j)
                if len(value)==4:
                    print("|"+repr("%.2f"%value[1]).center(width)),
                else:
                    print("|"+repr(" ").center(width)),

            print("|")
            for j in range(cols):
                value = self.get_value_cell(i,j)
                if len(value)==4:
                    print("|"+repr("%.2f"%value[2]).ljust(width/2)+repr("%.2f"%value[0]).rjust(width/2)),
                else:
                    if value[0] == None:    print("|"+repr("None").center(width)),
                    else:    print("|"+repr("%.2f"%value[0]).center(width)),
            print("|")
            for j in range(cols):
                value = self.get_value_cell(i,j)
                if len(value)==4:
                    print("|"+repr("%.2f"%value[3]).center(width)),
                else:
                    print("|"+repr(" ").center(width)),
            print("|")
        print("---------------------------------------------------------------------------------------------------------------")


    def to_utilities_and_policy(self):
        chars = {0: '>', 1: '^', 2: '<', 3: 'v', None: '.'}
        self.utilities = {}
        self.policy = {}
        for key in self.Q_dict:
            max_value = max(self.Q_dict[key])
            self.utilities[key] ="{0:.2f}".format(max_value)
            action = self.Q_dict[key].index(max_value)
            if len(self.Q_dict[key]) == 1:
                self.policy[key] = chars[None]
            else:
                self.policy[key] = chars[action]


def act_randomly(probability):
    return random.random() < probability

def to_grid(mapping):

        b = list(reversed([[mapping.get((x, y))
                           for x in range(4)]
                          for y in range(3)]))
        for x in range(3):
            for y in range(4):
                if b[x][y] != None:
                    b[x][y] = max(b[x][y])
        return np.array(b)


def Q_learning(agent,sim,no_of_trials):
    # delta = 1
    # while not(delta < sim.epsilon * (1 - sim.gamma) / sim.gamma):
    for i in range(no_of_trials):

        current_state = agent.init_state
        actual_policy_action = (0,1) # up

        # agent.to_utilities_and_policy()
        # U1 = agent.utilities
        #
        # no_of_trial = 0

        for state in agent.terminal_states:
            agent.Q_update(state,0,sim.R(state))

        # delta = 1
        # U = U1.copy()

        while True:
            if current_state in agent.terminal_states:
                break

            # no_of_trial += 1

            if act_randomly(agent.epsilon):
                action = agent.actions[random.randrange(0,4)]
            else:
                action = actual_policy_action

            new_state = sim.go_agent(current_state,action)
            agent.N_increment(current_state,action)
            alpha = 1.0/agent.N(current_state,action)

            Q_old = agent.Q(current_state,action)
            Q_new = [agent.f(agent.Q(new_state,a),agent.N(new_state,a)) for a in agent.actions]

            sample = sim.R(current_state)+sim.gamma*max(Q_new)
            Q_value = Q_old + alpha*(sample-Q_old)
            agent.Q_update(current_state,action,Q_value)

            current_state = new_state
            actual_policy_action = agent.actions[np.argmax(Q_new)]
            # agent.to_utilities_and_policy()
            # U1 = agent.utilities

        # U_val = 0
        # delta = 0
        # for s in agent.states:
        #     U_val = abs(float(U1[s])-float(U[s]))
        #     print("U",U_val)
            # delta = max(delta, U_val)
        #
        # print delta



    print("No of trials "+str(i+1))
    print("Epsilon "+str(agent.epsilon))
    agent.to_utilities_and_policy()
    print("Policy")
    sim.print_console(sim.to_grid(agent.policy))
    print("Utilities")
    sim.print_console(sim.to_grid(agent.utilities))
    agent.print_Q()

if __name__=='__main__':
    param = {}
    param['N'] = 4
    param['M'] = 4
    param['p1'] = 0.8 #probability go up
    param['p2'] = 0.1 #probability go right
    param['p3'] = 0.1 #probability go left
    param['r'] = -1.0
    param['gamma'] = 0.99
    param['epsilon'] = 0.0001

    param['F'] = [[2,0]]
    param['B'] = [[2,1,-20]]
    param['T'] = [[3,0,100]]

    ## special 5x5
    param = {}
    param['N'] = 5
    param['M'] = 5
    param['p1'] = 0.6 #probability go up
    param['p2'] = 0.2 #probability go right
    param['p3'] = 0.1 #probability go left
    param['r'] = -1
    param['gamma'] = 0.99
    param['epsilon'] = 0.001

    param['F'] = []
    param['B'] = [[1,2,-10],[1,3,-20],[2,1,-5],[2,3,-5],[3,1,-20],[3,2,-10]]
    param['T'] = [[0,0,-50],[0,4,100],[4,0,100],[4,4,-50]]

    sim = Simulator(param)
    print("*****World**************")
    sim.print_world()

    agent_param = {}
    agent_param['states'] = sim.states
    agent_param['actions'] = sim.actions(sim.states)
    agent_param['rows'] = sim.rows
    agent_param['cols'] = sim.cols
    agent_param['init_state'] = (2,2)
    agent_param['terminal_states'] = sim.terminals

    agent_param['epsilon'] = 0.25
    agent = Agent(agent_param)

    # agent_param['epsilon'] = 0.05
    # agent1 = Agent(agent_param)


    no_of_trials = 10000
    # print("*****Start Example 1*****")
    Q_learning(agent,sim,no_of_trials)
    # print('*****End Example 1*******')


    # no_of_trials = 10000
    # print("*****Start Example 2*****")
    # Q_learning(agent1,sim,no_of_trials)
    # print('*****End Example 2*******')