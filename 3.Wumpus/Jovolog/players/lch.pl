	/* -*- mode: Prolog; comment-column: 48 -*- */

/****************************************************************************
 *
 * Copyright (c) 2016 Łukasz Chojnacki
 *
 * I grant everyone the right to copy this program in whole or part, and
 * to use it for any purpose, provided the source is properly acknowledged.
 *
 * Udzielam kazdemu prawa do kopiowania tego programu w calosci lub czesci,
 * i wykorzystania go w dowolnym celu, pod warunkiem zacytowania zrodla.
 *
 ****************************************************************************/


% auxiliary initial action generating rule
act(Action, Knowledge) :-

	% To avoid looping on act/2.
	not(gameStarted),
	assert(gameStarted),

	% Creating initial knowledge
	worldSize(X,Y),				%this is given
	assert(myWorldSize(X,Y)),
	assert(myPosition(1, 1, east)),		%this we assume by default
	assert(myTrail([])),
	assert(haveGold(0)),
	assert(myWumpus([])),
	assert(myPits([])),
	act(Action, Knowledge).

% standard action generating rules
% this is our agent's algorithm, the rules will be tried in order
act(Action, Knowledge) :- exit_if_wumpus_pit(Action, Knowledge). %if at home with gold
act(Action, Knowledge) :- exit_if_home(Action, Knowledge). %if at home with gold
act(Action, Knowledge) :- go_back_step(Action, Knowledge). %if have gold elsewhere
act(Action, Knowledge) :- exit_if_steps(Action, Knowledge). %if at home with gold
act(Action, Knowledge) :- pick_up_gold(Action, Knowledge). %if just found gold
act(Action, Knowledge) :- turn_if_bump(Action, Knowledge).
act(Action, Knowledge) :- turn_left_if_wall(Action, Knowledge). %if against the wall
act(Action, Knowledge) :- turn_right_if_wall(Action, Knowledge). %if against the wall
act(Action,Knowledge) :-if_after_wumpus(Action,Knowledge). %if you fell wumpus
act(Action,Knowledge) :-if_wumpus(Action,Knowledge). %if you fell wumpus
act(Action,Knowledge) :-if_after_pit(Action,Knowledge). %if you fell wumpus
act(Action,Knowledge) :-if_pit(Action,Knowledge). %if you fell wumpus
act(Action, Knowledge) :- if_bored(Action, Knowledge). %if against the wall
act(Action, Knowledge) :- exploreRight(Action, Knowledge). 
act(Action, Knowledge) :- if_loop(Action, Knowledge). 
act(Action, Knowledge) :- else_move_on(Action, Knowledge). %otherwise


exit_if_wumpus_pit(Action, Knowledge) :-
	(stench;breeze),
	myPosition(1, 1, Orient),
	Action = exit,				%done game
	Knowledge = [exit_if_wumpus_pit].				%irrelevant but required

exit_if_home(Action, Knowledge) :-
	haveGold(NGolds), NGolds > 0,
	myPosition(1, 1, Orient),
	Action = exit,				%done game
	Knowledge = [exit_if_home].				%irrelevant but required


go_back_step(Action, Knowledge) :-
	%%% assuming we have just found gold:
	%%% 1. our last action must have been grab
	%%% 2. our previuos action must have been moveForward
	%%% 3. so we are initiating a turnback and then return:
	%%%    (a) pop grab from the stack
	%%%    (b) replace it by an artificial turnRight we have never
	%%%        executed, but we will be reversing by turning left
	%%%    (c) execute a turnRight now which together will turn us back
	%%% 4. after that we are facing back and can execute actions in reverse
	%%% 5. because of grab we can be sure this rule is executed exactly once
	haveGold(NGolds), NGolds > 0,
	myWorldSize(Max_X, Max_Y),
	myTrail(Trail),
	Trail = [ [grab,X,Y,Orient] | Trail_Tail ],
	New_Trail = [ [turnRight,X,Y,Orient] | Trail_Tail ], %Orient is misleading here
	Action = turnLeft,
    myWumpus(Wumpus),
	 myPits(Pits),
	Knowledge = [go_back_step,gameStarted,
	             haveGold(NGolds),
		     myWorldSize(Max_X, Max_Y),
		     myPosition(X, Y, Orient),
		     myWumpus(Wumpus),
		     myPits(Pits),
			 myTrail(New_Trail)].

go_back_step(Action, Knowledge) :-
	haveGold(NGolds), NGolds > 0,
	myWorldSize(Max_X, Max_Y),
	myTrail([ [Action,X,Y,Orient] | Trail_Tail ]),
	Action = moveForward,
    myWumpus(Wumpus),
	 myPits(Pits),
	Knowledge = [go_back_step1,gameStarted,
	             haveGold(NGolds),
		     myWorldSize(Max_X, Max_Y),
		     myPosition(X, Y, Orient),
		     myWumpus(Wumpus),
		     myPits(Pits),
			 myTrail(Trail_Tail)].

%%% backtracking a step can be moving or can be turning
go_back_step(Action, Knowledge) :- go_back_turn(Action, Knowledge).

go_back_turn(Action, Knowledge) :-
	haveGold(NGolds), NGolds > 0,
	myWorldSize(Max_X, Max_Y),
	myTrail([ [OldAct,X,Y,Orient] | Trail_Tail ]),
	%% if our previous action was a turn, we must reverse it now
	((OldAct=turnLeft,Action=turnRight);(OldAct=turnRight,Action=turnLeft)),
		     myWumpus(Wumpus),
 	myPits(Pits),
	Knowledge = [go_back_turn,gameStarted,
	             haveGold(NGolds),
		     myWorldSize(Max_X, Max_Y),
		     myPosition(X, Y, Orient),
		     myWumpus(Wumpus),
		     myPits(Pits),
			 myTrail(Trail_Tail)].

exit_if_steps(Action, Knowledge) :-
	Action = grab,			    %this is easy, we are sitting on it
	haveGold(NGolds),		    %we must know how many golds we have
	NewNGolds is NGolds + 1,
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	myTrail(Trail),
	len(Trail,N),
	N@>100,
	New_Trail = [ [Action,X,Y,Orient] | Trail ], %important to remember grab
	myWumpus(Wumpus),
 	myPits(Pits),
	Knowledge = [exit_if_steps,gameStarted,
	             haveGold(NewNGolds),
		     myWorldSize(Max_X, Max_Y),
		     myPosition(X, Y, Orient),	%the position stays the same
		     myWumpus(Wumpus),
		      myPits(Pits),
			myTrail(New_Trail)].

len([],0).
len([_|T],N) :- len(T,M), N is M+1.

pick_up_gold(Action, Knowledge) :-
	glitter,
	Action = grab,			    %this is easy, we are sitting on it
	haveGold(NGolds),		    %we must know how many golds we have
	NewNGolds is NGolds + 1,
	myWorldSize(Max_X, Max_Y),
	myPosition(X, Y, Orient),
	myTrail(Trail),
	New_Trail = [ [Action,X,Y,Orient] | Trail ], %important to remember grab
	myWumpus(Wumpus),
 	myPits(Pits),
	Knowledge = [pick_up_gold,gameStarted,
	             haveGold(NewNGolds),
		     myWorldSize(Max_X, Max_Y),
		     myPosition(X, Y, Orient),	%the position stays the same
		     myWumpus(Wumpus),
		      myPits(Pits),
			myTrail(New_Trail)].

turn_if_bump(Action, Knowledge) :-
	bump,
	Action = turnLeft,			%always successful
	myWorldSize(Max_X,Max_Y),
	myPosition(X, Y, Orient),		%this is a wrong position
	retractStep(X, Y, Orient, Real_X, Real_Y),	%since we bumped a wall
	shiftOrientLeft(Orient, NewOrient),		%always successful
	haveGold(NGolds),
	myTrail([ [Act1,X1,Y1,Orient1] | Trail_Tail ]),
	New_Trail = [ [Action,X1,Y1,Orient1] | Trail_Tail ],
	myWumpus(Wumpus),
 	myPits(Pits),
	Knowledge = [turn_if_bump,gameStarted,
					haveGold(NGolds),
	             myWorldSize(Max_X, Max_Y),
		     myPosition(Real_X, Real_Y, NewOrient),
			myWumpus(Wumpus),
		      myPits(Pits),
		     myTrail(New_Trail)].

retractStep(X, Y, east, Real_X, Y) :- Real_X is (X-1).
retractStep(X, Y, south, X, Real_Y) :- Real_Y is (Y+1).
retractStep(X, Y, west, Real_X, Y) :- Real_X is (X+1).
retractStep(X, Y, north, X, Real_Y) :- Real_Y is (Y-1).     

turn_left_if_wall(Action, Knowledge) :-
	myPosition(X, Y, Orient),
	myWorldSize(Max_X,Max_Y),
	againstWallLeft(X, Y, Orient, Max_X, Max_Y),
	Action = turnLeft,			%always successful
	shiftOrientLeft(Orient, NewOrient),		%always successful
	haveGold(NGolds),
	myTrail(Trail),
	New_Trail = [ [Action,X,Y,Orient] | Trail ],
	myWumpus(Wumpus),
 	myPits(Pits),
	Knowledge = [turn_left_if_wall,Action,
			gameStarted,
		     haveGold(NGolds),
	             myWorldSize(Max_X, Max_Y),
		     myPosition(X, Y, NewOrient),
		     myWumpus(Wumpus),
		     myPits(Pits),
			 myTrail(New_Trail)].

againstWallLeft(X, Y, Orient, Max_X, Max_Y) :- X = Max_X, Y = 1,     Orient = east.
againstWallLeft(X, Y, Orient, Max_X, Max_Y) :- X = Max_X, Y = Max_Y, Orient = north.
againstWallLeft(X, Y, Orient, Max_X, Max_Y) :- X = 1,     Y = Max_Y, Orient = west.
againstWallLeft(X, Y, Orient, Max_X, Max_Y) :- X = 1,     Y = 1,     Orient = south.

shiftOrientLeft(east, north).
shiftOrientLeft(north, west).
shiftOrientLeft(west, south).
shiftOrientLeft(south, east).
 
turn_right_if_wall(Action, Knowledge) :-
	myPosition(X, Y, Orient),
	myWorldSize(Max_X,Max_Y),
	againstWallRight(X, Y, Orient, Max_X, Max_Y),
	Action = turnRight,			%always successful
	shiftOrientRight(Orient, NewOrient),		%always successful
	haveGold(NGolds),
	myTrail(Trail),
	New_Trail = [ [Action,X,Y,Orient] | Trail ],
	myWumpus(Wumpus),
 	myPits(Pits),
	Knowledge = [turn_right_if_wall,gameStarted,
		     haveGold(NGolds),
	             myWorldSize(Max_X, Max_Y),
		     myPosition(X, Y, NewOrient),
		     myWumpus(Wumpus),
		     myPits(Pits),
			 myTrail(New_Trail)].

againstWallRight(X, Y, Orient, Max_X, Max_Y) :- X = Max_X, Y = 1,     Orient = south.
againstWallRight(X, Y, Orient, Max_X, Max_Y) :- X = Max_X, Y = Max_Y, Orient = east.
againstWallRight(X, Y, Orient, Max_X, Max_Y) :- X = 1,     Y = Max_Y, Orient = north.
againstWallRight(X, Y, Orient, Max_X, Max_Y) :- X = 1,     Y = 1,     Orient = west.

shiftOrientRight(east, south).
shiftOrientRight(north,east ).
shiftOrientRight(west, north).
shiftOrientRight(south, west).


if_after_wumpus(Action,Knowledge):- 
	stench,
	myTrail([ [Act,X,Y,Orient],[Act1,X1,Y1,Orient1] | Trail_Tail ]),
	(Act=turnLeft,Act1=turnLeft,Action=moveForward),%if smell strench turnLeft twice and move forward
	myWorldSize(Max_X,Max_Y),
	shiftOrientLeft(Orient, NewOrient),		%always successful
	forwardStep(X, Y, NewOrient, New_X, New_Y),
	haveGold(NGolds),
	myTrail(Trail),
	New_Trail = [ [Action,New_X,New_Y,NewOrient] | Trail ],
	myWumpus(Wumpus),
	NewWumpus=[[X,Y] | Wumpus],
	 myPits(Pits),
	Knowledge=[if_after_wumpus,gameStarted,
									haveGold(NGolds),
									myWorldSize(Max_X, Max_Y),
		     						myPosition(New_X, New_Y, NewOrient),
		     						myWumpus(NewWumpus),
		     						 myPits(Pits),
									myTrail(New_Trail)].

if_wumpus(Action,Knowledge):- % actual position of wumpus
	stench,
	Action=turnLeft,
	myWorldSize(Max_X,Max_Y),
	myPosition(X, Y, Orient),	
	shiftOrientLeft(Orient, NewOrient),		%always successful
	haveGold(NGolds),
	myTrail(Trail),
	New_Trail = [ [Action,X,Y,Orient] | Trail ],
	myWumpus(Wumpus),
	myPits(Pits),
	Knowledge=[if_wumpus,gameStarted,
									haveGold(NGolds),
									myWorldSize(Max_X, Max_Y),
		     						myPosition(X, Y, NewOrient),	%the position stays the same
									myWumpus(Wumpus),
									myPits(Pits),
					     			myTrail(New_Trail)].
		     						

if_after_pit(Action,Knowledge):- 
	breeze,
	myTrail([ [Act,X,Y,Orient],[Act1,X1,Y1,Orient1] | Trail_Tail ]),
	(Act=turnLeft,Act1=turnLeft,Action=moveForward),%if smell strench turnLeft twice and move forward
	myWorldSize(Max_X,Max_Y),
	shiftOrientLeft(Orient, NewOrient),		%always successful
	forwardStep(X, Y, NewOrient, New_X, New_Y),
	haveGold(NGolds),
	myTrail(Trail),
	New_Trail = [ [Action,New_X,New_Y,NewOrient] | Trail ],
	myWumpus(Wumpus),
	 myPits(Pits),
	NewPit=[[X,Y] | Pits],
	Knowledge=[if_after_pit,gameStarted,
									haveGold(NGolds),
									myWorldSize(Max_X, Max_Y),
		     						myPosition(New_X, New_Y, NewOrient),
		     						myWumpus(Wumpus),
		     						 myPits(NewPit),
									myTrail(New_Trail)].

if_pit(Action,Knowledge):- % actual position of wumpus
	breeze,
	Action=turnLeft,
	myWorldSize(Max_X,Max_Y),
	myPosition(X, Y, Orient),	
	shiftOrientLeft(Orient, NewOrient),		%always successful
	haveGold(NGolds),
	myTrail(Trail),
	New_Trail = [ [Action,X,Y,Orient] | Trail ],
	myWumpus(Wumpus),
	myPits(Pits),
	Knowledge=[if_pit,gameStarted,
									haveGold(NGolds),
									myWorldSize(Max_X, Max_Y),
		     						myPosition(X, Y, NewOrient),	%the position stays the same
									myWumpus(Wumpus),
									myPits(Pits),
					     			myTrail(New_Trail)].
		     						



if_bored(Action,Knowledge):- 
	(myTrail([ [Act,X,Y,Orient], [Act2,X2,Y2,Orient2],[Act3,X3,Y3,Orient3] | Trail_Tail ]),
	(Act=moveForward,Act2=turnLeft,Act3=turnLeft,Action=turnRight)),%if smell strench turnLeft twice and move forward
	myWorldSize(Max_X,Max_Y),
	shiftOrientRight(Orient, NewOrient),		%always successful
	haveGold(NGolds),
	myTrail(Trail),
	New_Trail = [ [Action,X,Y,Orient] | Trail ],
	myWumpus(Wumpus),
	 myPits(Pits),
	Knowledge=[if_bored,gameStarted,
									haveGold(NGolds),
									myWorldSize(Max_X, Max_Y),
		     						myPosition(X,Y, NewOrient),
		     						myWumpus(Wumpus),
		     						 myPits(Pits),
									myTrail(New_Trail)].
									
shiftOrientRight(east, south).
shiftOrientRight(north,east ).
shiftOrientRight(west, north).
shiftOrientRight(south, west).

exploreRight(Action,Knowledge):- 
	myWorldSize(Max_X,Max_Y),
	haveGold(NGolds),
	myTrail(Trail),
	myPosition(X, Y, Orient),	
	in_myTrail([moveForward,X,Y,Orient]),
	myTrail([ [Act1,X1,Y1,Orient1],[Act2,X2,Y2,Orient2] | Trail_Tail ]),
	((not(Act1=turnLeft),not(Act2=moveForward),not(in_myTrail([turnLeft,X,Y,Orient])),
	Action=turnLeft,shiftOrientLeft(Orient, NewOrient))
%	(	not(in_myTrail([turnRight,X,Y,Orient])),
%	Action=turnRight,shiftOrientRight(Orient, NewOrient))
%	(	in_myTrail([moveForward,X,Y,Orient]),
%	Action=moveForward,	myPosition(Xa, Ya, Orienta),forwardStep(Xa, Ya, Orienta, X, Y))
	),	
	New_Trail = [ [Action,X,Y,Orient] | Trail ],
	myWumpus(Wumpus),
	 myPits(Pits),
	Knowledge=[explore,
									gameStarted,
									haveGold(NGolds),
									myWorldSize(Max_X, Max_Y),
		     						myPosition(X,Y, NewOrient),
		     						myWumpus(Wumpus),
		     						 myPits(Pits),
									myTrail(New_Trail)].
								
in_myTrail(X) :- myTrail(L), member(X, L).

if_loop(Action,Knowledge):- 
	Action=moveForward,
	myWorldSize(Max_X,Max_Y),
	haveGold(NGolds),
	myPosition(X, Y, Orient),	
	myTrail([X1,Y1,Z1 | Trail_Tail ]),
	in_myTrail2(X1,Y1),
	duplicate(Trail_Tail, X1),
	duplicate(Trail_Tail, Y1),
	myTrail(Trail),
	forwardStep(X, Y, Orient, New_X, New_Y),
	New_Trail = [ [Action,X,Y,Orient] | Trail ], %important to remember grab
	myWumpus(Wumpus),
 	myPits(Pits),
	Knowledge = [loop,gameStarted,
	             haveGold(NGolds),
		     myWorldSize(Max_X, Max_Y),
		     myPosition(New_X, New_Y, Orient),	%the position stays the same
		     myWumpus(Wumpus),
		      myPits(Pits),
			myTrail(New_Trail)].
									
in_myTrail3(X,Y,Z) :- myTrail(L), member(X, L),
			myTrail(L), member(Y, L),
			myTrail(L), member(Z, L).
duplicate([First|Rest], Element) :-duplicate_1(Rest, First, Element).

duplicate_1([This|Rest], X, X) :- duplicate_2(Rest, This, X).
duplicate_1([This|Rest],_, X) :-  duplicate_1(Rest, This, X).

duplicate_2(_, X, X). % second occurrence
duplicate_2([This|Rest],_, X) :-     duplicate_2(Rest, This, X).

else_move_on(Action, Knowledge) :-
	Action = moveForward,			%this will fail on a wall
	haveGold(NGolds),
	myWorldSize(Max_X,Max_Y),
	myPosition(X, Y, Orient),
	forwardStep(X, Y, Orient, New_X, New_Y),
	myTrail(Trail),
	New_Trail = [ [Action,X,Y,Orient] | Trail ],
	 myWumpus(Wumpus),
	 myPits(Pits),
	Knowledge =[else_move_on,
				gameStarted,
		     haveGold(NGolds),
	         myWorldSize(Max_X, Max_Y),
		     myPosition(New_X, New_Y, Orient),
		     myWumpus(Wumpus),
			 myPits(Pits),
			myTrail(New_Trail)].

forwardStep(X, Y, east,  New_X, Y) :- New_X is (X+1).
forwardStep(X, Y, south, X, New_Y) :- New_Y is (Y-1).
forwardStep(X, Y, west,  New_X, Y) :- New_X is (X-1).
forwardStep(X, Y, north, X, New_Y) :- New_Y is (Y+1).

