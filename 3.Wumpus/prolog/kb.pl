gameStarted.
haveGold(0).
myWorldSize(7,7).
myPosition(0,3,west).
myWumpus([]).
myPits([[3,2],[2,4],[1,5],[3,2],[4,1]]).
myTrail([[moveForward,1,3,west],[moveForward,2,3,west],[turnLeft,2,3,north],[moveForward,2,2,north],[turnLeft,2,2,east],[moveForward,1,2,east],[turnLeft,1,2,south],[moveForward,1,3,south],[turnLeft,1,3,west],[moveForward,2,3,west],[turnLeft,2,3,north],[moveForward,2,2,north],[turnLeft,2,2,east],[moveForward,1,2,east],[turnLeft,1,2,south],[moveForward,1,3,south],[turnLeft,1,3,west],[moveForward,2,3,west],[turnLeft,2,3,north],[moveForward,2,2,north],[turnLeft,2,2,east],[moveForward,1,2,east],[turnLeft,1,2,south],[moveForward,1,3,south],[turnLeft,1,3,west],[moveForward,2,3,west],[turnLeft,2,3,north],[moveForward,2,2,north],[turnLeft,2,2,east],[moveForward,1,2,east],[turnLeft,1,2,south],[moveForward,1,3,south],[turnLeft,1,3,west],[moveForward,2,3,west],[turnLeft,2,3,north],[moveForward,2,2,north],[turnLeft,2,2,east],[moveForward,1,2,east],[turnLeft,1,2,south],[moveForward,1,3,south],[turnLeft,1,3,west],[moveForward,2,3,west],[turnLeft,2,3,north],[moveForward,2,2,north],[turnRight,2,2,west],[moveForward,2,2,west],[turnLeft,3,2,north],[turnLeft,3,2,east],[moveForward,2,2,east],[moveForward,1,2,east],[turnLeft,1,2,south],[moveForward,1,3,south],[turnLeft,1,3,west],[moveForward,2,3,west],[turnRight,2,3,south],[moveForward,2,3,south],[turnLeft,2,4,west],[turnLeft,2,4,north],[moveForward,2,3,north],[moveForward,2,2,north],[moveForward,2,1,north],[turnLeft,2,1,east],[moveForward,1,1,east],[turnLeft,1,1,south],[moveForward,1,2,south],[moveForward,1,3,south],[moveForward,1,4,south],[turnLeft,1,4,west],[turnRight,1,4,south],[moveForward,1,4,south],[turnLeft,1,5,west],[turnLeft,1,5,north],[moveForward,1,4,north],[moveForward,1,3,north],[moveForward,1,2,north],[moveForward,1,1,north],[turnRight,1,1,west],[moveForward,2,1,west],[moveForward,3,1,west],[turnRight,3,1,south],[moveForward,3,1,south],[turnLeft,3,2,west],[turnLeft,3,2,north],[moveForward,3,1,north],[turnRight,3,1,west],[moveForward,3,1,west],[turnLeft,4,1,north],[turnLeft,4,1,east],[moveForward,3,1,east],[moveForward,2,1,east],[moveForward,1,1,east]]).




shiftOrientLeft(east, north).
shiftOrientLeft(north, west).
shiftOrientLeft(west, south).
shiftOrientLeft(south, east).

shiftOrientRight(east, south).
shiftOrientRight(north,east ).
shiftOrientRight(west, north).
shiftOrientRight(south, west).

forwardStep(X, Y, east,  New_X, Y) :- New_X is (X+1).
forwardStep(X, Y, south, X, New_Y) :- New_Y is (Y-1).
forwardStep(X, Y, west,  New_X, Y) :- New_X is (X-1).
forwardStep(X, Y, north, X, New_Y) :- New_Y is (Y+1).


len([],0).
len([_|T],N) :-len(T,M), N is M+1.

in_myTrail(X) :- myTrail(L), member(X, L).

duplicate([First|Rest], Element) :-
    duplicate_1(Rest, First, Element).

duplicate_1([This|Rest], X, X) :- % first occurrence
    duplicate_2(Rest, This, X).
duplicate_1([This|Rest], _, X) :- % look further for first occurrence
    duplicate_1(Rest, This, X).

duplicate_2(_, X, X). % second occurrence
duplicate_2([This|Rest], _, X) :- % look further for second occurrence
    duplicate_2(Rest, This, X).

%findWumpus(X,Y,list) :- if_close(X,Y,list([W_X,W_Y]|Tail]]).
return_list_members([X], X):- !.
return_list_members([X|_], X).
return_list_members([_|T], X):-
  return_list_members(T, X).

if_close(X,Y,X_1,Y_1) :- X=:=(X_1-1),Y=:=Y_1,!;
                         X=:=(X_1+1),Y=:=Y_1,!;
                         X=:=X_1,Y=:=(Y_1+1),!;
                         X=:=X_1,Y=:=(Y_1-1).

add(X,List,[X|List]).

addWumpus(List,X,Y,X_Max,Y_Max,NewList) :- NewList=[[X,Y]|List].
